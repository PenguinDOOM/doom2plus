

This addon is not yet completed.


Compatibility list
・GZDoom ○

・Map mods ○ (If does not change or add enemies / weapons.)

・Multiplayer ○ (Please use DM patch when using Skulltag weapons.)

・Weapon mods × (If create a patch, compatibility will be preserved.)

・Enemy mods × (If create a patch, compatibility will be preserved.)

・Map with many breakable objects △ (Not recommended. Due to the possibility of performance degradation due to mass simultaneous destruction by radiation.)


